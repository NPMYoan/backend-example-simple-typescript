import express from 'express'

import diaryRouter from './routes/diaries'

const app = express()

const PORT = 3000

app.use(express.json())

app.use('/api/diaries', diaryRouter)

app.get('/', (_req, res) => {
  console.log('someone pinged here!!')
  res.send('pong')
})

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}!!`)
})
